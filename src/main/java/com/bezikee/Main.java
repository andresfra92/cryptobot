package com.bezikee;

import com.bezikee.common.AppResponse;
import com.bezikee.common.Registry;
import com.bezikee.common.entities.ExchangeInfo;
import com.bezikee.common.helpers.LoggerOps;
import com.bezikee.data_access_layer.repositories.binance.market.ExchangeInfoBinanceRepository;
import com.bezikee.service_layer.Portfolio;
import com.bezikee.service_layer.Runner;
import com.bezikee.service_layer.back_testing.BackTestService;
import com.bezikee.service_layer.strategies.Strategy;

import java.util.ArrayList;

public class Main {
    
    public static void main(String[] args) {
        Registry.API_KEY = args[0];
        Registry.SECRET_KEY = args[1];
        LoggerOps.info(Registry.class, "Starting program!");
        
        
        ExchangeInfoBinanceRepository exchangeInfoBinanceRepository =
                new ExchangeInfoBinanceRepository();
        AppResponse<ArrayList<ExchangeInfo>> responseExchangeInfo =
                exchangeInfoBinanceRepository.getAll();
        if (responseExchangeInfo.isSuccess()) {
            Registry.EXCHANGE_INFO = responseExchangeInfo.getPayload();
        }
        
        
       /* OrderBinanceRepository _repo = new OrderBinanceRepository();
        LoggerOps.debug(Main.class, "Executing test buy");
        AppResponse<Double> boughtAmount = _repo.placeMarketBuy(new SymbolPair(Symbol.BTC,
                Symbol.USDT), 11);
        
        
        LoggerOps.debug(Main.class, "Executing test sell");
        _repo.placeMarketSell(new SymbolPair(Symbol.BTC, Symbol.USDT), boughtAmount.getPayload());*/
        
        
        Portfolio port = new Portfolio();
        
        
        //if first argument is true, then is backtesting
        if (args.length > 2 && Boolean.parseBoolean(args[2])) {
            for (Strategy actual : port.strategies) {
                BackTestService service = new BackTestService(500, actual);
                service.execute();
            }
        } else {
            for (Strategy actual : port.strategies) {
                Runner runner = new Runner(actual);
                runner.start();
            }
            
        }
        
        
    }
}
