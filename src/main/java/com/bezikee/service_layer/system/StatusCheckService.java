package com.bezikee.service_layer.system;

import com.bezikee.common.AppResponse;
import com.bezikee.data_access_layer.repositories.binance.system.SystemBinanceRepository;

public class StatusCheckService {
    
    SystemBinanceRepository _systemBinanceRepository = new SystemBinanceRepository();
    public String output;
    
    public void execute() {
        AppResponse<String> response = _systemBinanceRepository.statusCheck();
        output = response.getPayload();
    }
}
