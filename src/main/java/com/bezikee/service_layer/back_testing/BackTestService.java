package com.bezikee.service_layer.back_testing;

import com.bezikee.common.AppResponse;
import com.bezikee.common.entities.Candlestick;
import com.bezikee.common.helpers.LoggerOps;
import com.bezikee.data_access_layer.repositories.binance.market.MarketBinanceRepository;
import com.bezikee.service_layer.plotting.PlotCandlesticksService;
import com.bezikee.service_layer.strategies.Strategy;

import java.sql.Timestamp;
import java.util.ArrayList;

public class BackTestService {
    
    private int _limit;
    private ArrayList<Candlestick> _candlesticks;
    private Timestamp _startDate;
    private Timestamp _endDate;
    private Strategy _strategy;
    
    public BackTestService(int limit, Timestamp startDate, Timestamp endDate, Strategy strategy) {
        _limit = limit;
        _startDate = startDate;
        _endDate = endDate;
        _strategy = strategy;
    }
    
    public BackTestService(int _limit, Strategy _strategy) {
        this._limit = _limit;
        this._strategy = _strategy;
    }
    
    public void execute() {
        LoggerOps.debug(this.getClass(),
                "Starting back testing: " + _strategy.getDescription());
        _fetchCandlesticks();
        
        
        _generatePlot();
    }
    
    private void _fetchCandlesticks() {
        MarketBinanceRepository _marketBinanceRepository = new MarketBinanceRepository();
        
        AppResponse<ArrayList<Candlestick>> responseCandlesticks =
                _marketBinanceRepository.getAllCandlesticksWith(_strategy.symbolPair.getString(),
                        _strategy.interval,
                        _startDate, _endDate, _limit + _strategy.getMaxLimit());
        _candlesticks = responseCandlesticks.getPayload();
    }
    
    
    private ArrayList<Boolean> _generateEntryPoints() {
        ArrayList<Boolean> output = new ArrayList<Boolean>();
        for (int i = 0; i < _limit; i++) {
            output.add(_strategy.rules.get(0).execute(new ArrayList<>(_candlesticks.subList(i,
                    _candlesticks.size()))));
        }
        return output;
    }
    
    private ArrayList<Boolean> _generateExitPoints() {
        ArrayList<Boolean> output = new ArrayList<Boolean>();
        for (int i = 0; i < _limit; i++) {
            output.add(_strategy.rules.get(1).execute(new ArrayList<>(_candlesticks.subList(i,
                    _candlesticks.size()))));
        }
        return output;
    }
    
    private void _generatePlot() {
        
        PlotCandlesticksService _plotCandlesticksService =
                new PlotCandlesticksService(_candlesticks, _generateExtraData(),
                        _generatePlotTitles(),
                        _generateEntryPoints(),
                        _generateExitPoints(),
                        _strategy.symbolPair.getString(), _limit);
        _plotCandlesticksService.execute();
    }
    
    private ArrayList<String> _generatePlotTitles() {
        ArrayList<String> output = new ArrayList<>();
        for (int i = 0; i < _strategy.calculatorsForPlot.size(); i++) {
            output.add(_strategy.calculatorsForPlot.get(i).getTitle());
        }
        return output;
    }
    
    private ArrayList<ArrayList<Double>> _generateExtraData() {
        
        ArrayList<ArrayList<Double>> output = new ArrayList<>();
        for (int i = 0; i < _strategy.calculatorsForPlot.size(); i++) {
            output.add(new ArrayList<>());
        }
        
        for (int i = 0; i < _limit; i++) {
            ArrayList<Candlestick> subArray = new ArrayList<>(_candlesticks.subList(i,
                    _candlesticks.size()));
            for (int j = 0; j < _strategy.calculatorsForPlot.size(); j++) {
                output.get(j).add(_strategy.calculatorsForPlot.get(j).execute(subArray));
            }
        }
        return output;
    }
}
