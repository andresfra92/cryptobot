package com.bezikee.service_layer.rules.moving_average_convergence_divergence;

import com.bezikee.common.entities.Candlestick;
import com.bezikee.service_layer.calculators.ExponentialMovingAverageDifferenceCalculator;
import com.bezikee.service_layer.calculators.MACDSimpleMovingAverageCalculator;
import com.bezikee.service_layer.rules.Rule;

import java.util.ArrayList;

public class MACDBearishCrossoverRule extends Rule {
    
    private ExponentialMovingAverageDifferenceCalculator _emaDifferenceCalculator;
    private MACDSimpleMovingAverageCalculator _MACDSmaCalculator;
    
    public MACDBearishCrossoverRule(int smallEmaSize, int bigEmaSize, int smaSize) {
        size = 1;
        _emaDifferenceCalculator = new ExponentialMovingAverageDifferenceCalculator(smallEmaSize,
                bigEmaSize);
        calculators.add(_emaDifferenceCalculator);
        _MACDSmaCalculator = new MACDSimpleMovingAverageCalculator(smallEmaSize, bigEmaSize,
                smaSize);
        calculators.add(_MACDSmaCalculator);
    }
    
    @Override
    public boolean execute(ArrayList<Candlestick> candlesticks) {
        ArrayList<Candlestick> subArraList = new ArrayList<>(candlesticks.subList(size,
                candlesticks.size()));
        return _MACDSmaCalculator.execute(candlesticks) >= _emaDifferenceCalculator.execute(candlesticks) &&
                _MACDSmaCalculator.execute(subArraList) <= _emaDifferenceCalculator.execute(subArraList);
        
        
    }
    
}
