package com.bezikee.service_layer.rules.simple_rules;

import com.bezikee.common.entities.Candlestick;
import com.bezikee.service_layer.calculators.SimpleMovingAverageCalculator;
import com.bezikee.service_layer.rules.Rule;

import java.util.ArrayList;

public class ClosePriceBelowMovingAverageRule extends Rule {
    
    
    private SimpleMovingAverageCalculator _smaCalculator;
    
    public ClosePriceBelowMovingAverageRule(int smaSize) {
        size = 0;
        
        _smaCalculator = new SimpleMovingAverageCalculator(smaSize);
        calculators.add(_smaCalculator);
    }
    
    @Override
    public boolean execute(ArrayList<Candlestick> candlesticks) {
        return candlesticks.get(0).closePrice < _smaCalculator.execute(candlesticks);
    }
    
}