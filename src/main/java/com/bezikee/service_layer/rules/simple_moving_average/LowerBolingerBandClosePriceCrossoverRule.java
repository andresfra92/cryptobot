package com.bezikee.service_layer.rules.simple_moving_average;

import com.bezikee.common.entities.Candlestick;
import com.bezikee.service_layer.calculators.LowerBollingerBandCalculator;
import com.bezikee.service_layer.rules.Rule;

import java.util.ArrayList;

public class LowerBolingerBandClosePriceCrossoverRule extends Rule {
    
    private LowerBollingerBandCalculator _lowerBollingerBandCalculator;
    
    public LowerBolingerBandClosePriceCrossoverRule(int smaSize, int stdDev) {
        size = 1;
        _lowerBollingerBandCalculator = new LowerBollingerBandCalculator(smaSize, stdDev);
        calculators.add(_lowerBollingerBandCalculator);
    }
    
    @Override
    public boolean execute(ArrayList<Candlestick> candlesticks) {
        
        double lowerBollingerBand = _lowerBollingerBandCalculator.execute(candlesticks);
        Candlestick last = candlesticks.get(0);
        
        return /*(last.closePrice <= lowerBollingerBand && last.openPrice <= lowerBollingerBand) ||
                (last.closePrice <= lowerBollingerBand && last.openPrice >= lowerBollingerBand) ||
                (last.closePrice >= lowerBollingerBand && last.openPrice <= lowerBollingerBand);
                */true;
        
        
    }
    
}
