package com.bezikee.service_layer.rules.simple_moving_average;

import com.bezikee.common.entities.Candlestick;
import com.bezikee.service_layer.calculators.ExponentialMovingAverageCalculator;
import com.bezikee.service_layer.calculators.SimpleMovingAverageCalculator;
import com.bezikee.service_layer.rules.Rule;

import java.util.ArrayList;

public class SimpleAndExponentialMovingAverageBearishCrossoverRule extends Rule {
    
    private SimpleMovingAverageCalculator _smaCalculator;
    private ExponentialMovingAverageCalculator _emaCalculator;
    
    public SimpleAndExponentialMovingAverageBearishCrossoverRule(int smaSize, int emaSize) {
        size = 1;
        _smaCalculator = new SimpleMovingAverageCalculator(smaSize);
        calculators.add(_smaCalculator);
        _emaCalculator = new ExponentialMovingAverageCalculator(emaSize);
        calculators.add(_emaCalculator);
    }
    
    @Override
    public boolean execute(ArrayList<Candlestick> candlesticks) {
        ArrayList<Candlestick> subArraList = new ArrayList<>(candlesticks.subList(size,
                candlesticks.size()));
        return _smaCalculator.execute(candlesticks) >= _emaCalculator.execute(candlesticks) &&
                _smaCalculator.execute(subArraList) <= _emaCalculator.execute(subArraList);
        
        
    }
    
}
