package com.bezikee.service_layer.rules.simple_moving_average;

import com.bezikee.common.entities.Candlestick;
import com.bezikee.service_layer.calculators.ExponentialMovingAverageCalculator;
import com.bezikee.service_layer.rules.Rule;

import java.util.ArrayList;

public class ExponentialMovingAverageBullishCrossoverRule extends Rule {
    
    private ExponentialMovingAverageCalculator _emaSmallCalculator;
    private ExponentialMovingAverageCalculator _emaBigCalculator;
    
    public ExponentialMovingAverageBullishCrossoverRule(int smallEmaSize, int bigEmaSize) {
        size = 1;
        _emaSmallCalculator = new ExponentialMovingAverageCalculator(smallEmaSize);
        calculators.add(_emaSmallCalculator);
        _emaBigCalculator = new ExponentialMovingAverageCalculator(bigEmaSize);
        calculators.add(_emaBigCalculator);
    }
    
    @Override
    public boolean execute(ArrayList<Candlestick> candlesticks) {
        ArrayList<Candlestick> subArraList = new ArrayList<>(candlesticks.subList(size,
                candlesticks.size()));
        return _emaSmallCalculator.execute(candlesticks) >= _emaBigCalculator.execute(candlesticks) &&
                _emaSmallCalculator.execute(subArraList) <= _emaBigCalculator.execute(subArraList);
        
        
    }
    
}
