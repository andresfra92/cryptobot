package com.bezikee.service_layer.rules.simple_moving_average;

import com.bezikee.common.entities.Candlestick;
import com.bezikee.service_layer.calculators.SimpleMovingAverageCalculator;
import com.bezikee.service_layer.rules.Rule;

import java.util.ArrayList;

public class SimpleMovingAverageBullishCrossoverRule extends Rule {
    
    private SimpleMovingAverageCalculator _smallSmaCalculator;
    private SimpleMovingAverageCalculator _bigSmaCalculator;
    
    public SimpleMovingAverageBullishCrossoverRule(int smallSmaSize, int bigSmaSize) {
        size = 1;
        _smallSmaCalculator = new SimpleMovingAverageCalculator(smallSmaSize);
        calculators.add(_smallSmaCalculator);
        _bigSmaCalculator = new SimpleMovingAverageCalculator(bigSmaSize);
        calculators.add(_bigSmaCalculator);
    }
    
    @Override
    public boolean execute(ArrayList<Candlestick> candlesticks) {
        ArrayList<Candlestick> subArraList = new ArrayList<>(candlesticks.subList(size,
                candlesticks.size()));
        return _smallSmaCalculator.execute(candlesticks) >= _bigSmaCalculator.execute(candlesticks) &&
                _smallSmaCalculator.execute(subArraList) <= _bigSmaCalculator.execute(subArraList);
        
        
    }
    
}
