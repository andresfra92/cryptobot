package com.bezikee.service_layer.rules.simple_moving_average;

import com.bezikee.common.entities.Candlestick;
import com.bezikee.service_layer.calculators.UpperBollingerBandCalculator;
import com.bezikee.service_layer.rules.Rule;

import java.util.ArrayList;

public class UpperBolingerBandClosePriceCrossoverRule extends Rule {
    
    private UpperBollingerBandCalculator _upperBollingerBandCalculator;
    
    double exitPrice = 0;
    double stopLossLevel = Double.NEGATIVE_INFINITY;
    boolean hadCrossover = false;
    
    public UpperBolingerBandClosePriceCrossoverRule(int smaSize, int stdDev) {
        size = 1;
        _upperBollingerBandCalculator = new UpperBollingerBandCalculator(smaSize, stdDev);
        calculators.add(_upperBollingerBandCalculator);
    }
    
    @Override
    public boolean execute(ArrayList<Candlestick> candlesticks) {
        
        double upperBollingerBand = _upperBollingerBandCalculator.execute(candlesticks);
        Candlestick last = candlesticks.get(0);
        
        if (hadCrossover) {
            exitPrice = last.closePrice;
            stopLossLevel = exitPrice * 0.95;
            
            return last.closePrice <= stopLossLevel;
        } else {
            if (last.closePrice >= upperBollingerBand && last.openPrice <= upperBollingerBand) {
                hadCrossover = true;
            }
            return false;
        }
        
    }
    
}
