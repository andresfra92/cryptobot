package com.bezikee.service_layer.rules;

import com.bezikee.common.entities.Candlestick;
import com.bezikee.service_layer.calculators.Calculator;

import java.util.ArrayList;

public abstract class Rule {
    
    public int size;
    public ArrayList<Calculator> calculators = new ArrayList<>();
    
    public abstract boolean execute(ArrayList<Candlestick> candlesticks);
    
}
