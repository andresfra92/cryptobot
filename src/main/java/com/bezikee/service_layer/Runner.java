package com.bezikee.service_layer;

import com.bezikee.common.AppResponse;
import com.bezikee.common.entities.Candlestick;
import com.bezikee.common.helpers.LoggerOps;
import com.bezikee.data_access_layer.repositories.binance.market.MarketBinanceRepository;
import com.bezikee.service_layer.strategies.Strategy;

import java.util.ArrayList;

public class Runner extends Thread {
    
    private Strategy strategy;
    
    public Runner(Strategy strategy) {
        this.strategy = strategy;
    }
    
    @Override
    public void run() {
        LoggerOps.debug(this.getClass(),
                "Starting runner: " + strategy.getDescription());
        MarketBinanceRepository repo = new MarketBinanceRepository();
        while (true) {
            LoggerOps.debug(this.getClass(),
                    "Looking for entry point : " + strategy.getDescription());
            AppResponse<ArrayList<Candlestick>> response =
                    repo.getAllCandlesticksWith(strategy.symbolPair.getString(),
                            strategy.interval, null, null, strategy.getMaxLimit());
            if (response.isSuccess()) {
                if (strategy.validate(response.getPayload())) {
                    strategy.triggerActionChain(response.getPayload());
                }
            }
            
            
            try {
                this.sleep(strategy.requestInterval.time);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            
        }
    }
    
    
}
