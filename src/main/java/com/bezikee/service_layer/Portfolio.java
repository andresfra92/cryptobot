package com.bezikee.service_layer;

import com.bezikee.common.entities.Interval;
import com.bezikee.common.entities.Symbol;
import com.bezikee.common.entities.SymbolPair;
import com.bezikee.service_layer.actions.Action;
import com.bezikee.service_layer.actions.AlternateEntryExitPointAction;
import com.bezikee.service_layer.calculators.Calculator;
import com.bezikee.service_layer.calculators.LowerBollingerBandCalculator;
import com.bezikee.service_layer.calculators.SimpleMovingAverageCalculator;
import com.bezikee.service_layer.calculators.UpperBollingerBandCalculator;
import com.bezikee.service_layer.rules.Rule;
import com.bezikee.service_layer.rules.simple_moving_average.LowerBolingerBandClosePriceCrossoverRule;
import com.bezikee.service_layer.strategies.Strategy;

import java.util.ArrayList;

public class Portfolio {
    public ArrayList<Strategy> strategies = new ArrayList<>();
    
    final private double _entryPrice = 11;
    final private double _defaultWinPercentage = 0.012;
    final private double _defaultStopPricePercentage = 0.005;
    final private double _defaultStopLimitPricePercentage = 0.004;
    
    public Portfolio() {
        //My initial portfolio
        ArrayList<Rule> rules;
        ArrayList<Calculator> calculators;
        ArrayList<Action> actions;
        //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* BTC/USDT *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-//
        //------------- 5 MIN ------------- //
        rules = new ArrayList<>();
        rules.add(new LowerBolingerBandClosePriceCrossoverRule(20, 2));
        
        
        calculators = new ArrayList<>();
        calculators.add(new SimpleMovingAverageCalculator(20));
        calculators.add(new LowerBollingerBandCalculator(20, 2));
        calculators.add(new UpperBollingerBandCalculator(20, 2));
        
        
        actions = new ArrayList<>();
        actions.add(new AlternateEntryExitPointAction(_entryPrice, 20, 2));
        
        
        ArrayList<Symbol> symbols = new ArrayList<Symbol>() {{
            /*add(Symbol.BTC);
            add(Symbol.ETH);
            add(Symbol.DOGE);
            add(Symbol.BNB);
            add(Symbol.ADA);*/
            add(Symbol.DASH);
            /*add(Symbol.LTC);
            add(Symbol.COMP);*/
        }};
        
        for (Symbol actual : symbols) {
            strategies.add(new Strategy("Bollinger bands.",
                    rules, calculators, new SymbolPair(actual, Symbol.USDT),
                    Interval.FIFTEEN_MINUTE,
                    Interval.FIFTEEN_MINUTE,
                    actions));
        }
        //------------- 5 MIN END ------------- //
        /*//------------- 15 MIN ------------- //
        rules = new ArrayList<>();
        rules.add(new MACDBullishCrossoverRule(12, 26, 9));
        
        
        calculators = new ArrayList<>();
        calculators.add(new ExponentialMovingAverageCalculator(20));
        calculators.add(new LowerBollingerBandCalculator(20, 2));
        
        
        actions = new ArrayList<>();
        actions.add(new AlternateEntryExitPointAction(_entryPrice, 12, 26, 9));
        
        strategies.add(new Strategy("Moving average convergence divergence: 12 26 9",
                rules, calculators, new SymbolPair(Symbol.BTC, Symbol.USDT),
                Interval.FIFTEEN_MINUTE,
                actions));
        //------------- 15 MIN END ------------- //
        //------------- 1 Hour ------------- //
        rules = new ArrayList<>();
        rules.add(new MACDBullishCrossoverRule(12, 26, 9));
        
        
        calculators = new ArrayList<>();
        calculators.add(new ExponentialMovingAverageCalculator(20));
        calculators.add(new LowerBollingerBandCalculator(20, 2));
        
        
        actions = new ArrayList<>();
        actions.add(new AlternateEntryExitPointAction(_entryPrice, 12, 26, 9));
        
        strategies.add(new Strategy("Moving average convergence divergence: 12 26 9",
                rules, calculators, new SymbolPair(Symbol.BTC, Symbol.USDT),
                Interval.ONE_HOUR,
                actions));
        //------------- 1 Hour END ------------- //
        //------------- 1 Day ------------- //
        rules = new ArrayList<>();
        rules.add(new MACDBullishCrossoverRule(12, 26, 9));
        
        
        calculators = new ArrayList<>();
        calculators.add(new ExponentialMovingAverageCalculator(20));
        calculators.add(new LowerBollingerBandCalculator(20, 2));
        
        
        actions = new ArrayList<>();
        actions.add(new AlternateEntryExitPointAction(_entryPrice, 12, 26, 9));
        
        strategies.add(new Strategy("Moving average convergence divergence: 12 26 9",
                rules, calculators, new SymbolPair(Symbol.BTC, Symbol.USDT),
                Interval.ONE_DAY,
                actions));
        //------------- 1 Day ------------- //
        //------------- 1 Week ------------- //
        rules = new ArrayList<>();
        rules.add(new MACDBullishCrossoverRule(12, 26, 9));
        
        
        calculators = new ArrayList<>();
        calculators.add(new ExponentialMovingAverageCalculator(20));
        calculators.add(new LowerBollingerBandCalculator(20, 2));
        
        
        actions = new ArrayList<>();
        actions.add(new AlternateEntryExitPointAction(_entryPrice, 12, 26, 9));
        
        strategies.add(new Strategy("Moving average convergence divergence: 12 26 9",
                rules, calculators, new SymbolPair(Symbol.BTC, Symbol.USDT),
                Interval.ONE_WEEK,
                actions));*/
        //------------- 1 Week ------------- //
        //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* BTC/USDT END *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-//
        
        
    }
}
