package com.bezikee.service_layer.calculators;

import com.bezikee.common.entities.Candlestick;

import java.util.ArrayList;

public class ExponentialMovingAverageDifferenceCalculator extends Calculator {
    
    private int _minSize;
    private int _maxSize;
    
    public ExponentialMovingAverageDifferenceCalculator(int inputSizeMin, int inputSizeMax) {
        _minSize = inputSizeMin;
        _maxSize = inputSizeMax;
        this.size = inputSizeMax;
        
    }
    
    public double execute(ArrayList<Candlestick> candlesticks) {
        ExponentialMovingAverageCalculator maxCalc =
                new ExponentialMovingAverageCalculator(_maxSize);
        ExponentialMovingAverageCalculator minCalc =
                new ExponentialMovingAverageCalculator(_minSize);
        
        return minCalc.execute(candlesticks) - maxCalc.execute(candlesticks);
        
    }
    
    @Override
    public String getTitle() {
        return "Exponential moving average difference: " + _maxSize + " - " + _minSize;
    }
}
