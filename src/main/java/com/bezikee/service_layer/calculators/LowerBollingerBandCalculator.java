package com.bezikee.service_layer.calculators;

import com.bezikee.common.entities.Candlestick;

import java.util.ArrayList;

public class LowerBollingerBandCalculator extends Calculator {
    
    
    private int stdDevMult;
    
    public LowerBollingerBandCalculator(int inputSize, int stdDevMult) {
        this.size = inputSize;
        this.stdDevMult = stdDevMult;
    }
    
    public double execute(ArrayList<Candlestick> candlesticks) {
        
        SimpleMovingAverageCalculator smaC = new SimpleMovingAverageCalculator(size);
        double basis = smaC.execute(candlesticks);
        StandardDeviationCalculator stdDevC = new StandardDeviationCalculator(size);
        double stdDev = stdDevMult * stdDevC.execute(candlesticks);
        
        return basis - stdDev;
        
    }
    
    @Override
    public String getTitle() {
        return "Lower Bollinger Band: " + size;
    }
}
