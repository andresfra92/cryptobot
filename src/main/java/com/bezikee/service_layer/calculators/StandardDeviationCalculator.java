package com.bezikee.service_layer.calculators;

import com.bezikee.common.entities.Candlestick;

import java.util.ArrayList;

public class StandardDeviationCalculator extends Calculator {
    
    
    public StandardDeviationCalculator(int inputSize) {
        this.size = inputSize;
    }
    
    public double execute(ArrayList<Candlestick> candlesticks) {
        
        double toReturn = 0;
        
        SimpleMovingAverageCalculator smac = new SimpleMovingAverageCalculator(size);
        double mean = smac.execute(candlesticks);
        
        for (int i = 0; i < size; i++) {
            toReturn = toReturn + Math.pow((candlesticks.get(i).closePrice - mean), 2);
        }
        toReturn = toReturn / size;
        toReturn = Math.sqrt(toReturn);
        return toReturn;
        
    }
    
    @Override
    public String getTitle() {
        return "Lower Bollinger Band: " + size;
    }
}
