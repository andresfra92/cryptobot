package com.bezikee.service_layer.calculators;

import com.bezikee.common.entities.Candlestick;

import java.util.ArrayList;

public class MACDSimpleMovingAverageCalculator extends Calculator {
    
    private ExponentialMovingAverageDifferenceCalculator _emaDifferenceCalculator;
    
    private int _smaSize;
    
    public MACDSimpleMovingAverageCalculator(int smallEmaSize, int bigEmaSize, int smaSize) {
        this.size = (2 * bigEmaSize) + smaSize;
        _smaSize = smaSize;
        _emaDifferenceCalculator = new ExponentialMovingAverageDifferenceCalculator(smallEmaSize,
                bigEmaSize);
    }
    
    public double execute(ArrayList<Candlestick> candlesticks) {
        double sum = 0;
        for (int i = 0; i < _smaSize; i++) {
            ArrayList<Candlestick> subArray = new ArrayList<>(candlesticks.subList(i,
                    candlesticks.size()));
            sum = sum + _emaDifferenceCalculator.execute(subArray);
        }
        return sum / _smaSize;
    }
    
    @Override
    public String getTitle() {
        return "Simple MACD moving average: " + size;
    }
}
