package com.bezikee.service_layer.calculators;

import com.bezikee.common.entities.Candlestick;

import java.util.ArrayList;

public class SimpleMovingAverageCalculator extends Calculator {
    
    
    public SimpleMovingAverageCalculator(int size) {
        this.size = size;
    }
    
    public double execute(ArrayList<Candlestick> candlesticks) {
        double sum = 0;
        for (int i = 0; i < size; i++) {
            sum = sum + candlesticks.get(i).closePrice;
        }
        return sum / size;
    }
    
    @Override
    public String getTitle() {
        return "Simple moving average: " + size;
    }
}
