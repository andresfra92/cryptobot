package com.bezikee.service_layer.calculators;

import com.bezikee.common.entities.Candlestick;

import java.util.ArrayList;

public class ExponentialMovingAverageCalculator extends Calculator {
    
    private float _weihtMultiplier;
    private int _actual;
    
    
    public ExponentialMovingAverageCalculator(int inputSize) {
        this.size = 2 * inputSize;
        _actual = inputSize + 1;
        _weihtMultiplier = (2f / (inputSize + 1));
        
    }
    
    public double execute(ArrayList<Candlestick> candlesticks) {
        if (_actual == 0) {
            SimpleMovingAverageCalculator smac = new SimpleMovingAverageCalculator(size / 2);
            double toReturn = smac.execute(candlesticks);
            _actual = (size / 2) + 1;
            return toReturn;
        } else {
            _actual--;
            ArrayList<Candlestick> subList =
                    new ArrayList<Candlestick>(candlesticks.subList(1,
                            candlesticks.size()));
            return (subList.get(0).closePrice * _weihtMultiplier) + (execute(subList) * (1 - _weihtMultiplier));
        }
    }
    
    @Override
    public String getTitle() {
        return "Exponential moving average: " + size / 2;
    }
}
