package com.bezikee.service_layer.calculators;

import com.bezikee.common.entities.Candlestick;

import java.util.ArrayList;

public abstract class Calculator {
    
    public int size;
    
    public abstract double execute(ArrayList<Candlestick> candlesticks);
    
    public abstract String getTitle();
}
