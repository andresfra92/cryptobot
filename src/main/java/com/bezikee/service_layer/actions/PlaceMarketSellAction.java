package com.bezikee.service_layer.actions;

import com.bezikee.common.AppResponse;
import com.bezikee.common.entities.Candlestick;
import com.bezikee.common.helpers.LoggerOps;
import com.bezikee.data_access_layer.repositories.binance.orders.OrderBinanceRepository;
import com.bezikee.service_layer.strategies.Strategy;

import java.util.ArrayList;

public class PlaceMarketSellAction extends Action {
    
    double _quantity;
    OrderBinanceRepository _repo = new OrderBinanceRepository();
    
    double _quantitySelled;
    
    public PlaceMarketSellAction(double quantity) {
        _quantity = quantity;
        _quantitySelled = 0;
    }
    
    
    @Override
    public void execute(Strategy strategy, ArrayList<Candlestick> candlesticks) {
        LoggerOps.info(this.getClass(),
                "Selling " + _quantity + " " + strategy.symbolPair.getString() + " @ " + candlesticks.get(0).closePrice + " aprox.");
        AppResponse<Double> response = _repo.placeMarketSell(strategy.symbolPair, _quantity);
        if (response.isSuccess()) {
            _quantitySelled = response.getPayload();
        } else {
            _quantitySelled = 0.0;
        }
    }
}
