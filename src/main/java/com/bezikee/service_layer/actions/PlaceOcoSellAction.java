package com.bezikee.service_layer.actions;

import com.bezikee.common.entities.Candlestick;
import com.bezikee.common.helpers.DecimalHelper;
import com.bezikee.common.helpers.ExchangeInfoHelper;
import com.bezikee.common.helpers.LoggerOps;
import com.bezikee.data_access_layer.repositories.binance.orders.OrderBinanceRepository;
import com.bezikee.service_layer.strategies.Strategy;

import java.math.RoundingMode;
import java.util.ArrayList;

public class PlaceOcoSellAction extends Action {
    
    double _dollarsToSell;
    double _ammountBought;
    double _pricePercentage;
    double _stopPricePercentage;
    double _stopLimitPricePercentage;
    
    OrderBinanceRepository _repo = new OrderBinanceRepository();
    
    public PlaceOcoSellAction(double dollarsToSell, double ammountBought, double pricePercentage,
                              double stopPricePercentage,
                              double stopLimitPricePercentage) {
        _dollarsToSell = dollarsToSell;
        _ammountBought = ammountBought;
        _pricePercentage = pricePercentage;
        _stopPricePercentage = stopPricePercentage;
        _stopLimitPricePercentage = stopLimitPricePercentage;
        
        
    }
    
    @Override
    public void execute(Strategy strategy, ArrayList<Candlestick> candlesticks) {
        
        int tickSize = ExchangeInfoHelper.getTickSize(strategy.symbolPair.getString());
        int stepSize = ExchangeInfoHelper.getStepSize(strategy.symbolPair.getString());
        double priceBoughtApprox = _dollarsToSell / _ammountBought;
        String quantity = DecimalHelper.setPrecision(_ammountBought * 0.998, stepSize,
                RoundingMode.DOWN);
        
        String price = DecimalHelper.setPrecision(priceBoughtApprox * (1 + _pricePercentage),
                tickSize,
                RoundingMode.UP);
        
        String stopPrice =
                DecimalHelper.setPrecision(priceBoughtApprox * (1 - _stopPricePercentage),
                        tickSize,
                        RoundingMode.DOWN);
        
        String stopLimitPrice =
                DecimalHelper.setPrecision(priceBoughtApprox * (1 - _stopLimitPricePercentage),
                        tickSize,
                        RoundingMode.DOWN);
        
        LoggerOps.info(this.getClass(),
                "Placing oco: "
                        + quantity + " " + strategy.symbolPair.getString() + " @ " + price +
                        " with Stop Loss " + stopPrice + " triggered at " + stopLimitPrice);
        _repo.placeOcoSell(strategy.symbolPair, quantity, price, stopPrice, stopLimitPrice);
    }
    
    
}
