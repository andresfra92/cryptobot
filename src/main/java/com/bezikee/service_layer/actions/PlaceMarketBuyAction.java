package com.bezikee.service_layer.actions;

import com.bezikee.common.AppResponse;
import com.bezikee.common.entities.Candlestick;
import com.bezikee.common.helpers.LoggerOps;
import com.bezikee.data_access_layer.repositories.binance.orders.OrderBinanceRepository;
import com.bezikee.service_layer.strategies.Strategy;

import java.util.ArrayList;

public class PlaceMarketBuyAction extends Action {
    
    double _quantity;
    OrderBinanceRepository _repo = new OrderBinanceRepository();
    
    double _quantityBought;
    
    public PlaceMarketBuyAction(double quantity) {
        _quantity = quantity;
        _quantityBought = 0;
    }
    
    
    @Override
    public void execute(Strategy strategy, ArrayList<Candlestick> candlesticks) {
        LoggerOps.info(this.getClass(),
                "Buying " + String.valueOf(_quantity) + " " + strategy.symbolPair.getString() +
                        " @ " + candlesticks.get(0).closePrice + " aprox.");
        AppResponse<Double> response = _repo.placeMarketBuy(strategy.symbolPair, _quantity);
        if (response.isSuccess()) {
            _quantityBought = response.getPayload();
        } else {
            _quantityBought = 0.0;
        }
    }
}
