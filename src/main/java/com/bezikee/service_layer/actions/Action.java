package com.bezikee.service_layer.actions;

import com.bezikee.common.entities.Candlestick;
import com.bezikee.service_layer.strategies.Strategy;

import java.util.ArrayList;

public abstract class Action {
    
    
    public abstract void execute(Strategy strategy, ArrayList<Candlestick> candlesticks);
}
