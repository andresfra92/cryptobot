package com.bezikee.service_layer.actions;

import com.bezikee.common.entities.Candlestick;
import com.bezikee.common.entities.Interval;
import com.bezikee.common.helpers.DecimalHelper;
import com.bezikee.common.helpers.ExchangeInfoHelper;
import com.bezikee.service_layer.rules.simple_moving_average.LowerBolingerBandClosePriceCrossoverRule;
import com.bezikee.service_layer.rules.simple_moving_average.UpperBolingerBandClosePriceCrossoverRule;
import com.bezikee.service_layer.strategies.Strategy;

import java.math.RoundingMode;
import java.util.ArrayList;

public class AlternateEntryExitPointAction extends Action {
    
    double _amountToBuy;
    double _amountToSell;
    boolean _buyMode = true;
    
    int _smaSize;
    int _stdDev;
    
    
    PlaceMarketBuyAction _buyAction;
    PlaceMarketSellAction _sellAction;
    
    public AlternateEntryExitPointAction(double _amountToBuy, int smaSize, int _stdDev) {
        this._amountToBuy = _amountToBuy;
        this._buyAction = new PlaceMarketBuyAction(_amountToBuy);
        
        _smaSize = smaSize;
        _stdDev = _stdDev;
    }
    
    public void execute(Strategy strategy, ArrayList<Candlestick> candlesticks) {
        
        if (_buyMode) {
            strategy.requestInterval = strategy.interval;
            _buyAction.execute(strategy, candlesticks);
            if (_buyAction._quantityBought != 0.0) {
                _amountToSell = _buyAction._quantityBought;
                _buyMode = false;
                strategy.requestInterval = Interval.ONE_SECOND;
                strategy.rules = new ArrayList<>();
                strategy.rules.add(new UpperBolingerBandClosePriceCrossoverRule(_smaSize, _stdDev));
            }
        } else {
            int stepSize = ExchangeInfoHelper.getStepSize(strategy.symbolPair.getString());
            _sellAction =
                    new PlaceMarketSellAction(Double.parseDouble(DecimalHelper.setPrecision(_amountToSell * 0.999,
                            stepSize,
                            RoundingMode.DOWN)));
            _sellAction.execute(strategy, candlesticks);
            strategy.rules = new ArrayList<>();
            strategy.rules.add(new LowerBolingerBandClosePriceCrossoverRule(_smaSize, _stdDev));
            _buyMode = true;
        }
        
        
    }
    
}
