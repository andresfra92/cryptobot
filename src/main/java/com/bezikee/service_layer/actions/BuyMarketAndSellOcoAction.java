package com.bezikee.service_layer.actions;

import com.bezikee.common.entities.Candlestick;
import com.bezikee.data_access_layer.repositories.binance.orders.OrderBinanceRepository;
import com.bezikee.service_layer.strategies.Strategy;

import java.util.ArrayList;

public class BuyMarketAndSellOcoAction extends Action {
    double _dollarsToSell;
    double _pricePercentage;
    double _stopPricePercentage;
    double _stopLimitPricePercentage;
    
    OrderBinanceRepository _repo = new OrderBinanceRepository();
    
    public BuyMarketAndSellOcoAction(double dollarsToSell, double pricePercentage,
                                     double stopPricePercentage,
                                     double stopLimitPricePercentage) {
        _dollarsToSell = dollarsToSell;
        _pricePercentage = pricePercentage;
        _stopPricePercentage = stopPricePercentage;
        _stopLimitPricePercentage = stopLimitPricePercentage;
        
        
    }
    
    @Override
    public void execute(Strategy strategy, ArrayList<Candlestick> candlesticks) {
        PlaceMarketBuyAction buyMarketAction = new PlaceMarketBuyAction(_dollarsToSell);
        buyMarketAction.execute(strategy, candlesticks);
        double amountBought = buyMarketAction._quantityBought;
        
        if (amountBought > 0) {
            PlaceOcoSellAction ocoSellAction = new PlaceOcoSellAction(_dollarsToSell,
                    amountBought, _pricePercentage, _stopPricePercentage,
                    _stopLimitPricePercentage);
            ocoSellAction.execute(strategy, candlesticks);
        }
        
        
    }
}
