package com.bezikee.service_layer.strategies;

import com.bezikee.common.entities.Candlestick;
import com.bezikee.common.entities.Interval;
import com.bezikee.common.entities.SymbolPair;
import com.bezikee.service_layer.actions.Action;
import com.bezikee.service_layer.calculators.Calculator;
import com.bezikee.service_layer.rules.Rule;

import java.util.ArrayList;

public class Strategy {
    public String name;
    public ArrayList<Rule> rules;
    public ArrayList<Calculator> calculatorsForPlot;
    public SymbolPair symbolPair;
    public Interval interval;
    public Interval requestInterval;
    
    public ArrayList<Action> actions;
    
    public Strategy(String name, ArrayList<Rule> rules, ArrayList<Calculator> calculatorsForPlot,
                    SymbolPair symbolPair, Interval interval, Interval requestInterval,
                    ArrayList<Action> actions) {
        this.name = name;
        this.rules = rules;
        this.calculatorsForPlot = calculatorsForPlot;
        this.symbolPair = symbolPair;
        this.interval = interval;
        this.requestInterval = requestInterval;
        this.actions = actions;
        
    }
    
    public Boolean validate(ArrayList<Candlestick> candlesticks) {
        boolean output = true;
        for (Rule actualRule : rules) {
            if (!actualRule.execute(candlesticks)) {
                output = false;
            }
        }
        return output;
    }
    
    public void triggerActionChain(ArrayList<Candlestick> candlesticks) {
        for (Action actual : actions) {
            actual.execute(this, candlesticks);
        }
    }
    
    
    public int getMaxLimit() {
        int calcMax = 0;
        int ruleMax = 0;
        for (Rule actualRule : rules) {
            if (actualRule.size > ruleMax) {
                ruleMax = actualRule.size;
            }
            for (Calculator actualCalculator : actualRule.calculators) {
                if (actualCalculator.size > calcMax) {
                    calcMax = actualCalculator.size;
                }
            }
            
        }
        return calcMax + ruleMax;
    }
    
    public String getDescription() {
        String sizes = " with sizes";
        for (Rule actualRule : rules) {
            for (Calculator actualCalculator : actualRule.calculators) {
                sizes = sizes + " " + Integer.toString(actualCalculator.size);
            }
        }
        return name + sizes + " -> (" + symbolPair.getString() + ", " + interval.value + ")";
    }
    
    
}
