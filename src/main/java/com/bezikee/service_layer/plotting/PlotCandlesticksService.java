package com.bezikee.service_layer.plotting;

import com.bezikee.common.entities.Candlestick;
import org.knowm.xchart.OHLCChart;
import org.knowm.xchart.OHLCChartBuilder;
import org.knowm.xchart.OHLCSeries;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.style.Styler;

import java.awt.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PlotCandlesticksService {
    
    private ArrayList<Candlestick> _candlesticks;
    private ArrayList<ArrayList<Double>> _extraPlots;
    private ArrayList<String> _extraPlotsTitles;
    private ArrayList<Boolean> _entryPoints;
    private ArrayList<Boolean> _exitPoints;
    private String _symbol;
    private int _limit;
    
    
    public PlotCandlesticksService(ArrayList<Candlestick> candlesticks,
                                   ArrayList<ArrayList<Double>> extraPlots,
                                   ArrayList<String> extraPlotsTitles,
                                   ArrayList<Boolean> entryPoints,
                                   ArrayList<Boolean> exitPoints,
                                   String symbol, int limit) {
        
        _candlesticks = candlesticks;
        _extraPlots = extraPlots;
        _entryPoints = entryPoints;
        _exitPoints = exitPoints;
        _symbol = symbol;
        _extraPlotsTitles = extraPlotsTitles;
        _limit = limit;
    }
    
    public void execute() {
        new SwingWrapper<OHLCChart>(getChart()).displayChart();
    }
    
    public OHLCChart getChart() {
        
        OHLCChart chart = new OHLCChartBuilder().width(1800).height(1400).title(_symbol).build();
        
        chart.getStyler().setLegendPosition(Styler.LegendPosition.OutsideS);
        chart.getStyler().setLegendLayout(Styler.LegendLayout.Horizontal);
        chart.getStyler().setDatePattern("hh:mm dd-MMM-yyyy");
        
        List<Date> xData = new ArrayList<Date>();
        List<Double> openData = new ArrayList<Double>();
        List<Double> highData = new ArrayList<Double>();
        List<Double> lowData = new ArrayList<Double>();
        List<Double> closeData = new ArrayList<Double>();
        List<ArrayList<Double>> extraPlotsData = new ArrayList<ArrayList<Double>>();
        List<Double> entryPointsData = new ArrayList<Double>();
        List<Double> exitPointsData = new ArrayList<Double>();
        
        for (int i = 0; i < _extraPlots.size(); i++) {
            extraPlotsData.add(new ArrayList<>());
        }
        
        populateData(xData, openData, highData, lowData, closeData, extraPlotsData,
                entryPointsData, exitPointsData);
        
        chart.addSeries("Candlesticks", xData, openData, highData, lowData, closeData)
                .setOhlcSeriesRenderStyle(OHLCSeries.OHLCSeriesRenderStyle.Candle).setDownColor(Color.RED).setUpColor(new Color(76, 153, 0));
        chart.getStyler().setToolTipsEnabled(true);
        chart.addSeries("Entry Points", xData, entryPointsData).setMarkerColor(Color.black).setLineColor(Color.black);
        chart.addSeries("Exit Points", xData, exitPointsData).setMarkerColor(Color.blue).setLineColor(Color.blue);
        
        for (int i = 0; i < extraPlotsData.size(); i++) {
            chart.addSeries(_extraPlotsTitles.get(i), xData, extraPlotsData.get(i));
        }
        
        
        return chart;
    }
    
    private void populateData(
            List<Date> xData,
            List<Double> openData,
            List<Double> highData,
            List<Double> lowData,
            List<Double> closeData,
            List<ArrayList<Double>> extraPlotsData,
            List<Double> entryPointsData,
            List<Double> exitPointsData
    ) {
        
        for (int i = 0; i < _limit; i++) {
            Candlestick actual = _candlesticks.get(i);
            xData.add(actual.closeTime);
            openData.add(actual.openPrice);
            highData.add(actual.highPrice);
            lowData.add(actual.lowPrice);
            closeData.add(actual.closePrice);
            
            
           /* openData.add(0.0);
            highData.add(0.0);
            lowData.add(0.0);
            closeData.add(0.0);*/
            
            for (int j = 0; j < _extraPlots.size(); j++) {
                extraPlotsData.get(j).add(_extraPlots.get(j).get(i));
            }
            
            
            if (_entryPoints.get(i)) {
                entryPointsData.add(actual.closePrice);
            } else {
                entryPointsData.add(_extraPlots.get(0).get(i));
            }
            
            if (_exitPoints.get(i)) {
                exitPointsData.add(actual.closePrice);
            } else {
                exitPointsData.add(_extraPlots.get(0).get(i));
            }
        }
    }
    
    
}
