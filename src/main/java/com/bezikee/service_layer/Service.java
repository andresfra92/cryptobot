package com.bezikee.service_layer;

public abstract class Service {
    protected abstract void execute();
}
