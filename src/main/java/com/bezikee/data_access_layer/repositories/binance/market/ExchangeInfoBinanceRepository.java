package com.bezikee.data_access_layer.repositories.binance.market;

import com.bezikee.common.AppResponse;
import com.bezikee.common.entities.ExchangeInfo;
import com.bezikee.data_access_layer.daos.binance.market.ExchangeInfoBinanceDao;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.util.ArrayList;

public class ExchangeInfoBinanceRepository {
    
    ExchangeInfoBinanceDao exchangeinfoBinanceDao = new ExchangeInfoBinanceDao();
    
    public AppResponse<ArrayList<ExchangeInfo>> getAll() {
        ArrayList<ExchangeInfo> output = new ArrayList<ExchangeInfo>();
        try {
            output = exchangeinfoBinanceDao.getAll();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return AppResponse.success(output);
    }
}
