package com.bezikee.data_access_layer.repositories.binance.orders;

import com.bezikee.common.AppResponse;
import com.bezikee.common.entities.OrderSide;
import com.bezikee.common.entities.SymbolPair;
import com.bezikee.data_access_layer.daos.binance.orders.OrderBinanceDao;
import com.mashape.unirest.http.exceptions.UnirestException;

public class OrderBinanceRepository {
    
    OrderBinanceDao _dao = new OrderBinanceDao();
    
    public AppResponse<Double> placeMarketBuy(SymbolPair symbolPair, double quoteOrderQty) {
        
        try {
            Double priceBought = _dao.NewMarketOrder(symbolPair.getString(), OrderSide.BUY,
                    quoteOrderQty);
            return AppResponse.success(priceBought);
        } catch (UnirestException e) {
        
        }
        return AppResponse.success();
    }
    
    public AppResponse placeOcoSell(SymbolPair symbol, String quantity, String price,
                                    String stopPrice, String stopLimitPrice) {
        
        try {
            _dao.NewOcoOrder(symbol.getString(), OrderSide.SELL, quantity, price, stopPrice,
                    stopLimitPrice);
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return AppResponse.success();
    }
    
    public AppResponse<Double> placeMarketSell(SymbolPair symbolPair, double orderQty) {
        
        try {
            Double priceBought = _dao.NewMarketOrder(symbolPair.getString(), OrderSide.SELL,
                    orderQty);
            return AppResponse.success(priceBought);
        } catch (UnirestException e) {
        
        }
        return AppResponse.success();
    }
}
