package com.bezikee.data_access_layer.repositories.binance.market;

import com.bezikee.common.AppResponse;
import com.bezikee.common.entities.Candlestick;
import com.bezikee.common.entities.Interval;
import com.bezikee.data_access_layer.daos.binance.market.CandlestickBinanceDao;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.sql.Timestamp;
import java.util.ArrayList;

public class MarketBinanceRepository {

    CandlestickBinanceDao candlestickBinanceDao = new CandlestickBinanceDao();

    public AppResponse<ArrayList<Candlestick>> getAllCandlesticksWith(String symbol, Interval interval, Timestamp startTime, Timestamp endTime, int limit){
        ArrayList<Candlestick> output = new ArrayList<Candlestick>();
        try{
            output = candlestickBinanceDao.getAllWith(symbol,interval,startTime,endTime,limit);
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return AppResponse.success(output);
    }
}
