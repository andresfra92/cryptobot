package com.bezikee.data_access_layer.repositories.binance.system;

import com.bezikee.common.AppResponse;
import com.bezikee.common.entities.Candlestick;
import com.bezikee.common.entities.Interval;
import com.bezikee.data_access_layer.daos.binance.market.CandlestickBinanceDao;
import com.bezikee.data_access_layer.daos.binance.system.SystemBinanceDao;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.sql.Timestamp;
import java.util.ArrayList;

public class SystemBinanceRepository {

    SystemBinanceDao systemBinanceDao = new SystemBinanceDao();

    public AppResponse<String> statusCheck(){
        String output;
        try{
            output = systemBinanceDao.statusCheck();
        } catch (UnirestException e) {
            output = e.toString();
            e.printStackTrace();
        }
        return AppResponse.success(output);
    }
}
