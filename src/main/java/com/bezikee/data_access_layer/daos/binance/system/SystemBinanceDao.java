package com.bezikee.data_access_layer.daos.binance.system;

import com.bezikee.common.http_client.BinanceHttpClient;
import com.mashape.unirest.http.HttpMethod;
import com.mashape.unirest.http.exceptions.UnirestException;

public class SystemBinanceDao {
    
    BinanceHttpClient httpClient = BinanceHttpClient.getInstance();
    
    public String statusCheck() throws UnirestException {
        return httpClient.request(BinanceHttpClient.STATUS_CHECK_URL, null, HttpMethod.GET).toString();
    }
}
