package com.bezikee.data_access_layer.daos.binance.market;

import com.bezikee.common.entities.Candlestick;
import com.bezikee.common.entities.Interval;
import com.bezikee.common.http_client.BinanceHttpClient;
import com.mashape.unirest.http.HttpMethod;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONArray;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;

public class CandlestickBinanceDao {
    
    BinanceHttpClient httpClient = BinanceHttpClient.getInstance();
    
    public ArrayList<Candlestick> getAllWith(String symbol, Interval interval,
                                             Timestamp startTime, Timestamp endTime, int limit) throws UnirestException {
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        
        parameters.put("symbol", symbol);//TODO better way to handle symbols
        parameters.put("interval", interval.value);
        
        if (startTime != null) {
            parameters.put("startTime", startTime.getTime());
        }
        if (endTime != null) {
            parameters.put("endTime", endTime.getTime());
        }
        
        if (limit != 0) {
            parameters.put("limit", limit);
        }
        
        ArrayList<Candlestick> output = new ArrayList<Candlestick>();
        HttpResponse<JsonNode> response = httpClient.request(BinanceHttpClient.CANDLESTICK_URL,
                parameters,
                HttpMethod.GET);
        for (int i = response.getBody().getArray().length() - 1; i >= 0; i--) {
            JSONArray actual = response.getBody().getArray().getJSONArray(i);
            
            
            output.add(new Candlestick(
                    new Timestamp((long) actual.get(0)),
                    new Timestamp((long) actual.get(6)),
                    Double.parseDouble((String) actual.get(1)),
                    Double.parseDouble((String) actual.get(4)),
                    Double.parseDouble((String) actual.get(2)),
                    Double.parseDouble((String) actual.get(3)),
                    Double.parseDouble((String) actual.get(5)),
                    Double.parseDouble((String) actual.get(7)),
                    (int) actual.get(8),
                    Double.parseDouble((String) actual.get(9)),
                    Double.parseDouble((String) actual.get(10)),
                    Double.parseDouble((String) actual.get(11)))
            
            );
            
            
        }
        return output;
        
    }
    
    
}
