package com.bezikee.data_access_layer.daos.binance.orders;

import com.bezikee.common.entities.OrderResponseType;
import com.bezikee.common.entities.OrderSide;
import com.bezikee.common.entities.OrderTimeInForce;
import com.bezikee.common.entities.OrderType;
import com.bezikee.common.helpers.DateHelper;
import com.bezikee.common.helpers.DecimalHelper;
import com.bezikee.common.helpers.ExchangeInfoHelper;
import com.bezikee.common.helpers.HmacHelper;
import com.bezikee.common.http_client.BinanceHttpClient;
import com.mashape.unirest.http.HttpMethod;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.math.RoundingMode;
import java.util.HashMap;

public class OrderBinanceDao {
    
    BinanceHttpClient httpClient = BinanceHttpClient.getInstance();
    
    public Double NewMarketOrder(String symbol, OrderSide orderSide, double quantity) throws UnirestException {
        
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("symbol", symbol);
        parameters.put("side", orderSide.toString());
        parameters.put("type", OrderType.MARKET.toString());
        
        parameters.put("newOrderRespType", OrderResponseType.RESULT.toString());
        parameters.put("timestamp", DateHelper.now().getTime());
        
        int stepSize = ExchangeInfoHelper.getStepSize(symbol);
        
        if (orderSide == OrderSide.BUY) {
            
            parameters.put(
                    "quoteOrderQty",
                    DecimalHelper.setPrecision(
                            quantity,
                            ExchangeInfoHelper.getTickSize(symbol), RoundingMode.DOWN));
        } else {
            parameters.put("quantity",
                    DecimalHelper.setPrecision(
                            quantity,
                            ExchangeInfoHelper.getStepSize(symbol), RoundingMode.DOWN));
        }
        
        
        addSignedParameter(parameters);
        
        HttpResponse<JsonNode> response = httpClient.request(BinanceHttpClient.PLACE_ORDER_URL,
                parameters, HttpMethod.POST);
        
        if (response.getStatus() == 200) {
            return Double.parseDouble(response.getBody().getObject().getString("executedQty"));
        } else {
            return 0.0;
        }
        
        
    }
    
    
    public JsonNode NewOcoOrder(String symbol, OrderSide orderSide, String quantity, String price,
                                String stopPrice, String stopLimitPrice) throws UnirestException {
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("symbol", symbol);
        parameters.put("side", orderSide.toString());
        parameters.put("quantity", quantity);
        parameters.put("price", price);
        parameters.put("stopPrice", stopPrice);
        parameters.put("stopLimitPrice", stopLimitPrice);
        parameters.put("stopLimitTimeInForce", OrderTimeInForce.GTC.toString());
        parameters.put("timestamp", DateHelper.now().getTime());
        parameters.put("newOrderRespType", OrderResponseType.ACK.toString());
        
        addSignedParameter(parameters);
        
        HttpResponse<JsonNode> response =
                httpClient.request(BinanceHttpClient.PLACE_OCO_ORDER_URL, parameters,
                        HttpMethod.POST);
        
        return response.getBody();
        
    }
    
    
    private void addSignedParameter(HashMap<String, Object> parameters) {
        String toSign = "";
        
        for (Object actualKey : parameters.keySet()) {
            toSign = toSign + actualKey + "=" + parameters.get(actualKey).toString() + "&";
        }
        toSign = toSign.substring(0, toSign.length() - 1);
        
        parameters.put("signature", HmacHelper.sign(toSign));
    }
}
