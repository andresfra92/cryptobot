package com.bezikee.data_access_layer.daos.binance.market;

import com.bezikee.common.entities.ExchangeInfo;
import com.bezikee.common.entities.Symbol;
import com.bezikee.common.entities.SymbolPair;
import com.bezikee.common.http_client.BinanceHttpClient;
import com.mashape.unirest.http.HttpMethod;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ExchangeInfoBinanceDao {
    
    BinanceHttpClient httpClient = BinanceHttpClient.getInstance();
    
    public ArrayList<ExchangeInfo> getAll() throws UnirestException {
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        
        
        ArrayList<ExchangeInfo> output = new ArrayList<ExchangeInfo>();
        HttpResponse<JsonNode> response = httpClient.request(BinanceHttpClient.EXCHANGE_INFO_URL,
                parameters,
                HttpMethod.GET);
        JSONArray symbols = (JSONArray) response.getBody().getObject().get("symbols");
        for (int i = 0; i < symbols.length(); i++) {
            JSONObject actual = symbols.getJSONObject(i);
            String symbolPair = actual.getString("symbol");
            JSONArray filters = actual.getJSONArray("filters");
            String stepSizeString = filters.getJSONObject(2).getString("stepSize");
            String tickSizeString = filters.getJSONObject(0).getString("tickSize");
            
            output.add(
                    new ExchangeInfo(new SymbolPair(new Symbol(symbolPair.substring(0, 3)),
                            new Symbol(symbolPair.substring(3))),
                            Double.parseDouble(tickSizeString),
                            Double.parseDouble(stepSizeString)));
            
        }
        return output;
        
    }
    
    
}
