package com.bezikee.common.http_client;


import com.bezikee.common.Registry;
import com.bezikee.common.helpers.LoggerOps;
import com.mashape.unirest.http.HttpMethod;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.HttpRequest;

import java.util.HashMap;
import java.util.concurrent.Future;

public class BinanceHttpClient extends MyHttpClient {
    
    private int _weight = 0;
    final protected String baseUrl = "https://api.binance.com";
    
    public final static String CANDLESTICK_URL = "/api/v3/klines";
    public final static String STATUS_CHECK_URL = "/api/v3/ping";
    public final static String PLACE_ORDER_URL = "/api/v3/order";
    public final static String PLACE_ORDER_URL_TEST = "/api/v3/order/test";
    public final static String PLACE_OCO_ORDER_URL = "/api/v3/order/oco";
    public final static String EXCHANGE_INFO_URL = "/api/v3/exchangeInfo";
    
    
    HashMap<String, String> headers = new HashMap<String, String>() {{
        put("application-type", "application/x-www-form-urlencoded");
        put(" X-MBX-APIKEY", Registry.API_KEY);
    }};
    
    private static BinanceHttpClient instance = null;
    
    
    public static BinanceHttpClient getInstance() {
        if (instance == null) {
            return new BinanceHttpClient();
        } else {
            return instance;
        }
    }
    
    @Override
    Future<HttpResponse<JsonNode>> asyncRequest(String extraUrl,
                                                HashMap<String, Object> parameters,
                                                HttpMethod method) {
        return null;
        
    }
    
    //TODO http response rubust up, with response codes.
    @Override
    public HttpResponse<JsonNode> request(String extraUrl, HashMap<String, Object> parameters,
                                          HttpMethod method) throws UnirestException {
        LoggerOps.debug(this.getClass(), "Executing Binance request at: " + " Weight=" + _weight);
        HttpRequest request;
        switch (method) {
            case GET:
                request = Unirest.get(baseUrl + extraUrl).headers(headers).queryString(parameters);
                break;
            case POST:
                request = Unirest.post(baseUrl + extraUrl).headers(headers).queryString(parameters);
                break;
            case DELETE:
                request =
                        Unirest.delete(baseUrl + extraUrl).headers(headers).queryString(parameters);
                break;
            case PUT:
                request = Unirest.put(baseUrl + extraUrl).headers(headers).queryString(parameters);
                break;
            default:
                request = Unirest.get(baseUrl + extraUrl).headers(headers).queryString(parameters);
            
        }
        HttpResponse<JsonNode> response = request.asJson();
        _weight = Integer.parseInt(response.getHeaders().get("x-mbx-used-weight").get(0));
        if (response.getStatus() != 200) {
            LoggerOps.error(this.getClass(),
                    response.getStatusText() + ": " + response.getBody().toString());
        }
        return response;
    }
    
}
