package com.bezikee.common.http_client;

import com.mashape.unirest.http.HttpMethod;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.util.HashMap;
import java.util.concurrent.Future;

public abstract class MyHttpClient {
    protected String baseUrl;
    HashMap<String, Object> headers;
    
    
    abstract Future<HttpResponse<JsonNode>> asyncRequest(String extraUrl,
                                                         HashMap<String, Object> parameters,
                                                         HttpMethod method);
    
    abstract HttpResponse<JsonNode> request(String extraUrl, HashMap<String, Object> parameters,
                                            HttpMethod method) throws UnirestException;
    
    
}
