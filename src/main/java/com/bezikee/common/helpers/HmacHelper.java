package com.bezikee.common.helpers;

import com.bezikee.common.Registry;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class HmacHelper {
    
    public static String sign(String toSign) {
        String digest = null;
        String algo = "HmacSHA256";
        try {
            SecretKeySpec key = new SecretKeySpec((Registry.SECRET_KEY).getBytes(), algo);
            Mac mac = Mac.getInstance(algo);
            mac.init(key);
            
            byte[] bytes = mac.doFinal(toSign.getBytes());
            
            StringBuffer hash = new StringBuffer();
            for (int i = 0; i < bytes.length; i++) {
                String hex = Integer.toHexString(0xFF & bytes[i]);
                if (hex.length() == 1) {
                    hash.append('0');
                }
                hash.append(hex);
            }
            digest = hash.toString();
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            e.printStackTrace();
            
        }
        return digest;
    }
}
