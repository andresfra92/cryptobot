package com.bezikee.common.helpers;

import java.sql.Timestamp;
import java.time.Instant;

public class DateHelper {
    
    public static Timestamp now() {
        return new Timestamp(Instant.now().toEpochMilli());
    }
    
    
}
