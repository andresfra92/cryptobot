package com.bezikee.common.helpers;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class DecimalHelper {
    
    
    public static String setPrecision(double input, int precision, RoundingMode mode) {
        String output =
                BigDecimal.valueOf(input)
                        .setScale(precision, mode)
                        .toPlainString();
        return output;
    }
}
