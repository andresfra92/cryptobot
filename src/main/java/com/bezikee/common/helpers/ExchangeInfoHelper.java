package com.bezikee.common.helpers;

import com.bezikee.common.Registry;
import com.bezikee.common.entities.ExchangeInfo;

public class ExchangeInfoHelper {
    
    
    public static int getStepSize(String inputPair) {
        for (ExchangeInfo actual : Registry.EXCHANGE_INFO) {
            if (actual.symbolPair.getString().equals(inputPair)) {
                String splitted = Double.toString(actual.stepSize).split("\\.")[1];
                if (splitted.equals("0")) {
                    return 0;
                } else if (splitted.contains("-")) {
                    return Integer.parseInt(splitted.substring(splitted.length() - 1));
                    
                } else {
                    
                    return splitted.length();
                }
            }
        }
        return 0;
    }
    
    public static int getTickSize(String inputPair) {
        for (ExchangeInfo actual : Registry.EXCHANGE_INFO) {
            if (actual.symbolPair.getString().equals(inputPair)) {
                String splitted = Double.toString(actual.tickSize).split("\\.")[1];
                if (splitted.equals("0")) {
                    return 0;
                } else if (splitted.contains("-")) {
                    return Integer.parseInt(splitted.substring(splitted.length() - 1));
                    
                } else {
                    
                    return splitted.length();
                }
            }
        }
        return 0;
    }
}
