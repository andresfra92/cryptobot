package com.bezikee.common.helpers;


import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class LoggerOps {
    static Logger logger;
    
    public static void trace(Class classToLog, String log) {
        
        logger = LogManager.getLogger(classToLog);
        logger.trace(log);
    }
    
    public static void debug(Class classToLog, String log) {
        logger = LogManager.getLogger(classToLog);
        logger.debug(log);
        
    }
    
    public static void error(Class classToLog, String log) {
        logger = LogManager.getLogger(classToLog);
        logger.error(log);
    }
    
    public static void info(Class classToLog, String log) {
        logger = LogManager.getLogger(classToLog);
        logger.info(log);
    }
    
    public static void warn(Class classToLog, String log) {
        logger = LogManager.getLogger(classToLog);
        logger.warn(log);
    }
}
