package com.bezikee.common.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Date;

import static com.bezikee.common.gson.UnixEpochDateTypeAdapter.getUnixEpochDateTypeAdapter;
import static com.bezikee.common.gson.UnixEpochTimestampTypeAdapter.getUnixEpochTimestampTypeAdapter;

public class GsonOps {
    
    private static final Gson gson = new GsonBuilder()
            .enableComplexMapKeySerialization()
            .registerTypeAdapter(Date.class, getUnixEpochDateTypeAdapter())
            .registerTypeAdapter(Date.class, getUnixEpochTimestampTypeAdapter())
            .create();
    
    public static String toJson(Object obj) {
        return gson.toJson(obj);
    }
    
    public static Object fromJson(String json, Class classInput) {
        return gson.fromJson(json, classInput);
    }
}
