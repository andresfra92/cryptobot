package com.bezikee.common.gson;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.sql.Timestamp;


final class UnixEpochTimestampTypeAdapter
        extends TypeAdapter<Timestamp> {
    
    private static final TypeAdapter<Timestamp> unixEpochTimestampTypeAdapter =
            new UnixEpochTimestampTypeAdapter();
    
    private UnixEpochTimestampTypeAdapter() {
    }
    
    static TypeAdapter<Timestamp> getUnixEpochTimestampTypeAdapter() {
        return unixEpochTimestampTypeAdapter;
    }
    
    @Override
    public Timestamp read(final JsonReader in)
            throws IOException {
        // this is where the conversion is performed
        return new Timestamp(in.nextLong());
    }
    
    @Override
    @SuppressWarnings("resource")
    public void write(final JsonWriter out, final Timestamp value)
            throws IOException {
        // write back if necessary or throw UnsupportedOperationException
        if (value != null) {
            out.value(value.getTime());
        } else {
            out.nullValue();
        }
    }
    
}
