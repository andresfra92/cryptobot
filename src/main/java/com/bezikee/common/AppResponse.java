package com.bezikee.common;

public class AppResponse<T> {

    final private boolean _success;
    final private T _payload;


    public AppResponse(boolean status, T payload) {
        _success = status;
        _payload = payload;
    }

    public static <T> AppResponse<T> success(T payload) {
        return new AppResponse<T>(true, payload);
    }

    public static AppResponse success() {
        return new AppResponse(true, null);
    }

    public static <T> AppResponse<T> failure(T payload) {
        return new AppResponse<T>(false, payload);
    }

    public boolean isSuccess() {
        return _success;
    }

    public T getPayload() {
        return _payload;
    }

}
