package com.bezikee.common.entities;

public class SymbolPair {
    
    private Symbol symbol1;
    private Symbol symbol2;
    
    public SymbolPair(Symbol symbol1, Symbol symbol2) {
        this.symbol1 = symbol1;
        this.symbol2 = symbol2;
    }
    
    public String getString() {
        return symbol1.symbol + symbol2.symbol;
    }
}
