package com.bezikee.common.entities;

public enum OrderTimeInForce {
    GTC,
    IOC,
    FOK
}
