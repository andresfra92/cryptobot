package com.bezikee.common.entities;

public class Symbol {
    
    public static Symbol BTC = new Symbol("BTC");
    public static Symbol ETH = new Symbol("ETH");
    public static Symbol USDT = new Symbol("USDT");
    public static Symbol COMP = new Symbol("COMP");
    public static Symbol BNB = new Symbol("BNB");
    public static Symbol ADA = new Symbol("ADA");
    public static Symbol XRP = new Symbol("XRP");
    public static Symbol SUSHI = new Symbol("SUSHI");
    public static Symbol LTC = new Symbol("LTC");
    public static Symbol BTM = new Symbol("BTM");
    public static Symbol DAI = new Symbol("DAI");
    public static Symbol DASH = new Symbol("DASH");
    public static Symbol DOGE = new Symbol("DOGE");
    public static Symbol GNT = new Symbol("GNT");
    public static Symbol HOT = new Symbol("HOT");
    public static Symbol IOST = new Symbol("IOST");
    public static Symbol STEEM = new Symbol("STEEM");
    public static Symbol XMR = new Symbol("XMR");
    
    public String symbol;
    
    public Symbol(String symbol) {
        this.symbol = symbol;
    }
}
