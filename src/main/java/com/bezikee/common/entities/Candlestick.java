package com.bezikee.common.entities;

import java.sql.Timestamp;

public class Candlestick {
    
    public Timestamp openTime;
    public Timestamp closeTime;
    public double openPrice;
    public double closePrice;
    public double highPrice;
    public double lowPrice;
    public double volume;
    public double quoteAssetVolume;
    public double numberOfTrades;
    public double takerBuyBaseAssetVolume;
    public double takerBuyQuoteAssetVolume;
    public double ignore;
    
    public Candlestick(Timestamp openTime, Timestamp closeTime, double openPrice, double closePrice,
                       double highPrice, double lowPrice, double volume, double quoteAssetVolume,
                       double numberOfTrades, double takerBuyBaseAssetVolume,
                       double takerBuyQuoteAssetVolume, double ignore) {
        this.openTime = openTime;
        this.closeTime = closeTime;
        this.openPrice = openPrice;
        this.closePrice = closePrice;
        this.highPrice = highPrice;
        this.lowPrice = lowPrice;
        this.volume = volume;
        this.quoteAssetVolume = quoteAssetVolume;
        this.numberOfTrades = numberOfTrades;
        this.takerBuyBaseAssetVolume = takerBuyBaseAssetVolume;
        this.takerBuyQuoteAssetVolume = takerBuyQuoteAssetVolume;
        this.ignore = ignore;
    }
    
    public boolean isRed() {
        return openPrice > closePrice;
    }
}
