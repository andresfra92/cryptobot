package com.bezikee.common.entities;

public class ExchangeInfo {
    public SymbolPair symbolPair;
    public double tickSize;
    public double stepSize;
    
    public ExchangeInfo(SymbolPair symbolPair, double tickSize, double stepSize) {
        this.symbolPair = symbolPair;
        this.tickSize = tickSize;
        this.stepSize = stepSize;
    }
}
