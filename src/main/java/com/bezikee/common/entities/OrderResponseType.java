package com.bezikee.common.entities;

public enum OrderResponseType {
    ACK, RESULT, FULL
}
