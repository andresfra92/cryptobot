package com.bezikee.common.entities;

public class Interval {
    public static Interval ONE_SECOND = new Interval("1s", 1000);
    public static Interval ONE_MINUTE = new Interval("1m", 1000 * 60);
    public static Interval THREE_MINUTE = new Interval("3m", 1000 * 60 * 3);
    public static Interval FIVE_MINUTE = new Interval("5m", 1000 * 60 * 5);
    public static Interval FIFTEEN_MINUTE = new Interval("15m", 1000 * 60 * 15);
    public static Interval THIRTY_MINUTE = new Interval("30m", 1000 * 60 * 30);
    public static Interval ONE_HOUR = new Interval("1h", 1000 * 60 * 60);
    public static Interval TWO_HOUR = new Interval("2h", 1000 * 60 * 60 * 2);
    public static Interval FOUR_HOUR = new Interval("4h", 1000 * 60 * 60 * 4);
    public static Interval SIX_HOUR = new Interval("6h", 1000 * 60 * 60 * 6);
    public static Interval EIGHT_HOUR = new Interval("8h", 1000 * 60 * 60 * 8);
    public static Interval TWELVE_HOUR = new Interval("12h", 1000 * 60 * 60 * 12);
    public static Interval ONE_DAY = new Interval("1d", 1000 * 60 * 60 * 24);
    public static Interval TWO_DAY = new Interval("2d", 1000 * 60 * 60 * 24 * 2);
    public static Interval ONE_WEEK = new Interval("1w", 1000 * 60 * 60 * 24 * 7);
    public static Interval ONE_MONTH = new Interval("1M", 1000 * 60 * 60 * 24 * 30);
    
    public String value;
    public long time;
    
    public Interval(String value, long time) {
        this.value = value;
        this.time = time;
    }
}
