package com.bezikee.common.entities;

public enum OrderSide {
    BUY,
    SELL
}
