package com.bezikee.common;

import com.bezikee.common.entities.ExchangeInfo;

import java.util.ArrayList;

public class Registry {
    
    
    public static String BASE_RESOURCE_PATH = "/home/ec2-user/cryptobot";
    /*public static String BASE_RESOURCE_PATH =
    "D:/Developing/Projects/Bezikee/cryptobot/src/main" +
            "/resources";*/
    public static String IMAGES_PATH = BASE_RESOURCE_PATH + "/images";
    
    public static String API_KEY;
    public static String SECRET_KEY;
    public static ArrayList<ExchangeInfo> EXCHANGE_INFO;
}
