package com.bezikee.common.helpers;

import com.bezikee.common.AppResponse;
import com.bezikee.common.Registry;
import com.bezikee.common.entities.ExchangeInfo;
import com.bezikee.data_access_layer.repositories.binance.market.ExchangeInfoBinanceRepository;
import junit.framework.TestCase;

import java.util.ArrayList;

public class ExchangeInfoHelperTest extends TestCase {
    
    public void setUp() throws Exception {
        super.setUp();
        ExchangeInfoBinanceRepository exchangeInfoBinanceRepository =
                new ExchangeInfoBinanceRepository();
        AppResponse<ArrayList<ExchangeInfo>> responseExchangeInfo =
                exchangeInfoBinanceRepository.getAll();
        if (responseExchangeInfo.isSuccess()) {
            Registry.EXCHANGE_INFO = responseExchangeInfo.getPayload();
        }
        
    }
    
    public void testGetTickSize() {
        assertEquals(5, ExchangeInfoHelper.getTickSize("DOGEUSDT"));
        assertEquals(2, ExchangeInfoHelper.getTickSize("BTCUSDT"));
        assertEquals(6, ExchangeInfoHelper.getTickSize("HOTUSDT"));
        assertEquals(3, ExchangeInfoHelper.getTickSize("SNXUSDT"));
    }
    
    public void testGetStepSize() {
        assertEquals(1, ExchangeInfoHelper.getStepSize("DOGEUSDT"));
        assertEquals(6, ExchangeInfoHelper.getStepSize("BTCUSDT"));
        assertEquals(0, ExchangeInfoHelper.getStepSize("HOTUSDT"));
        assertEquals(3, ExchangeInfoHelper.getStepSize("SNXUSDT"));
    }
}