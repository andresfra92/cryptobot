package com.bezikee.common.helpers;

import junit.framework.TestCase;

import java.math.RoundingMode;

public class DecimalHelperTest extends TestCase {
    
    public void setUp() throws Exception {
        super.setUp();
    }
    
    public void testSetPrecision() {
        assertEquals("0.0000007", DecimalHelper.setPrecision(0.0000007131351, 7,
                RoundingMode.DOWN));
        assertEquals("0.0000007", DecimalHelper.setPrecision(7.131351E-7, 7, RoundingMode.DOWN));
    }
}